﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUp : MonoBehaviour
{

    public float velx;
    private float moveHorizontal;
    private Vector3 positionActual;
    public float maxx;
    public Transform posBall;

    // Update is called once per frame
    void Update()
    {
        if (posBall.position.y >= 0)
        {
            if (posBall.position.y > transform.position.y)
            {
                positionActual.y = 1.0f;
            }
            else if (posBall.position.y < transform.position.y)
            {
                positionActual.y = 0f;
            }
            if (posBall.position.x > transform.position.x)
            {
                moveHorizontal = -1.0f;
            }
            else if (posBall.position.x < transform.position.x)
            {
                moveHorizontal = 1.0f;
            }
        }
        else if (posBall.position.y < 0)
        {
            if (posBall.position.y > transform.position.y)
            {
                positionActual.y = 0f;
            }
            else if (posBall.position.y < transform.position.y)
            {
                positionActual.y = 0f;
            }
            if (posBall.position.x > transform.position.x)
            {
                moveHorizontal = 0f;
            }
            else if (posBall.position.x < transform.position.x)
            {
                moveHorizontal = 0f;
            }
        }

        transform.Translate(0, velx * moveHorizontal * Time.deltaTime, 0);

        positionActual = transform.position;
        if (positionActual.x > maxx)
        {
            positionActual.x = maxx;
        }

        else if (positionActual.x < -maxx)
        {
            positionActual.x = -maxx;
        }
     
        transform.position = positionActual;
    }
}
