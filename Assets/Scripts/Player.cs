﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float vely;
    private float moveVertical;
    private Vector3 positionActual;
    public float maxy;
	
	// Update is called once per frame
	void Update () {
        moveVertical = Input.GetAxis("Vertical");
        
        transform.Translate(0, vely * moveVertical * Time.deltaTime, 0);

        positionActual = transform.position;

        if(positionActual.y > maxy)
        {
            positionActual.y = maxy;
        }

        else if(positionActual.y < -maxy)
        {
            positionActual.y = -maxy;
        }

        transform.position = positionActual;

    }
}
