﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    public Vector2 speed;
    public float maxVel;
    Vector2 posBall;

	// Use this for initialization
	void Start () {
        posBall = new Vector2(transform.position.x, transform.position.y);
    }
	
	// Update is called once per frame
	void Update () {
        transform.Translate(speed.x * Time.deltaTime, speed.y * Time.deltaTime, 0);
        
	}

    void OnTriggerEnter(Collider other)
    {
        speed.x *= 1.25f;
        speed.y *= 1.25f;
        if(speed.x > maxVel)
        {
            speed.x = maxVel;
        }
        else if(speed.y > maxVel)
        {
            speed.y = maxVel;
        }

        if(speed.x < -maxVel)
        {
            speed.x = -maxVel;
        }
        else if(speed.y < -maxVel)
        {
            speed.y = -maxVel;
        }

        switch(other.tag)
        {
            case "Player":
                speed.x = -speed.x;
                break;
            case "Enemy":
                speed.x = -speed.x;
                break;
            case "Up":
                transform.position = posBall;
                speed.y = UnityEngine.Random.Range(-8.0f, 8.0f);
                speed.x = UnityEngine.Random.Range(-8.0f, 8.0f);
                break;
            case "Down":
                transform.position = posBall;
                speed.y = UnityEngine.Random.Range(3.0f, 5.0f);
                speed.x = UnityEngine.Random.Range(3.0f, 5.0f);
                break;
            case "Left":
                transform.position = posBall;
                speed.y = UnityEngine.Random.Range(3.0f, 5.0f);
                speed.x = UnityEngine.Random.Range(3.0f, 5.0f);
                break;
            case "Right":
                transform.position = posBall;
                speed.y = UnityEngine.Random.Range(3.0f, 5.0f);
                speed.x = UnityEngine.Random.Range(3.0f, 5.0f);
                break;
            case "EnemyDown":
                speed.y = -speed.y;
                break;
            case "EnemyUp":
                speed.y = -speed.y;
                break;
        }
    }
}
