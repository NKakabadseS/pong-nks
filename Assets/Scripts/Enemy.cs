﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    
    public float vely;
    private float moveVertical;
    private Vector3 positionActual;
    public float maxy;
    public Transform posBall;
    int visionEnemy;

    // Update is called once per frame
    void Update () {
        if(posBall.position.x >= 0)
        {
            if (posBall.position.y > transform.position.y)
            {
                moveVertical = 1.0f;
            }
            else if (posBall.position.y < transform.position.y)
            {
                moveVertical = -1.0f;
            }
            if (posBall.position.x > transform.position.x)
            {
                positionActual.x = 1.0f;
            }
            else if (posBall.position.x < transform.position.x)
            {
                positionActual.x = 0f;
            }
        }
        else if(posBall.position.x < 0)
        {
            if (posBall.position.y > transform.position.y)
            {
                moveVertical = 0f;
            }
            else if (posBall.position.y < transform.position.y)
            {
                moveVertical = 0f;
            }
            if (posBall.position.x > transform.position.x)
            {
                positionActual.x = 0f;
            }
            else if (posBall.position.x < transform.position.x)
            {
                positionActual.x = 0f;
            }
        }        
        
        transform.Translate(0, vely * moveVertical * Time.deltaTime, 0);

        positionActual = transform.position;
        if(positionActual.y > maxy)
        {
            positionActual.y = maxy;
        }

        else if(positionActual.y < -maxy)
        {
            positionActual.y = -maxy;
        }

        transform.position = positionActual;               
    }
}
